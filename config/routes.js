export default [
  {
    path: '/',
    component: '../layouts/BlankLayout',
    routes: [
      {
        path: '/user',
        component: '../layouts/UserLayout',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './User/login',
          },
        ],
      },
      {
        path: '/',
        component: '../layouts/SecurityLayout',
        routes: [
          {
            path: '/',
            component: '../layouts/BasicLayout',
            authority: ['admin', 'user'],
            routes: [
              {
                path: '/',
                redirect: '/welcome',
              },
              {
                path: '/welcome',
                name: 'welcome',
                icon: 'smile',
                component: './Welcome',
              },
              {
                path: '/admin',
                name: 'admin',
                icon: 'crown',
                component: './Admin',
                authority: ['admin'],
                routes: [
                  {
                    path: '/admin/sub-page',
                    name: 'sub-page',
                    icon: 'smile',
                    component: './Welcome',
                    authority: ['admin'],
                  },
                ],
              },
              {
                path: '/dashboard',
                name: 'dashboard',
                icon: 'dashboard',
                routes: [
                  {
                    path: '/',
                    redirect: '/dashboard/analysis',
                  },
                  {
                    name: 'analysis',
                    icon: 'smile',
                    path: '/dashboard/analysis',
                    component: './dashboard/analysis',
                  },
                  {
                    name: 'monitor',
                    icon: 'smile',
                    path: '/dashboard/monitor',
                    component: './dashboard/monitor',
                  },
                  {
                    path: '/dashboard/settings',
                    name: 'settings',
                    icon: 'smile',
                    //component: './user/Dashboard/Catalog',                    
                    routes:[
                      {
                      path: '/dashboard/settings/store',
                      name: 'store',
                      icon: 'smile',
                      routes:[{
                        path: '/dashboard/settings/store/category',
                        name: 'category',
                        icon: 'smile',
                        component: './dashboard/settings/store/category',
                      },
                      {
                        path: '/dashboard/settings/store/product',
                        name: 'product',
                        icon: 'smile',
                        component: './dashboard/settings/store/product',
                      },                      
                      {
                        path: '/dashboard/settings/store/consignment',
                        name: 'consignment',
                        icon: 'smile',
                        component: './dashboard/settings/store/consignment',
                      },
                      ],
  
                    },
                    ],
                  },
                  
                ],
              },
              {
                name: 'list.table-list',
                icon: 'table',
                path: '/list',
                component: './TableList',
              },
              {
                component: './404',
              },
            ],
          },
          {
            component: './404',
          },
        ],
      },
    ],
  },
  {
    component: './404',
  },
];
