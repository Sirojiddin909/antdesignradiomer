import request from '@/utils/request';
import { ACCESS_TOKEN } from '@/utils/constants';
export async function fakeAccountLogin(params) {
  return request('/api/auth/signin', {
    method: 'POST',
    data: params,
  });
}
export async function getFakeCaptcha(mobile) {
  return request(`/api/login/captcha?mobile=${mobile}`);
}

export const logOut = async () => {
  window.localStorage.removeItem(ACCESS_TOKEN);
};
