import React, { useState } from "react";
import styles from "./index.less";
import { Tree } from "antd";
import { DataNode } from "./data";
import request from "@/utils/request";


export interface CategoryTreeProps{
 
  onSelect?: (selectedKeys: any, info: any) => void;
 
}
  
  const initTreeDate: DataNode[] = [
    // {title:"Category",key:"null"},
  ];

  function updateTreeData(list: DataNode[], key: React.Key, children: DataNode[]): DataNode[] {
      return list.map(node => {
      if (node.key === key) {
        return {
          ...node,
          children,
        };
      } else if (node.children) {
        return {
          ...node,
          children: updateTreeData(node.children, key, children),
        };
      }
      return node;
    });
  }
const CategoryTree : React.FC<CategoryTreeProps> = (props) => {

  const [treeData, setTreeData] = useState(initTreeDate);
  const { onSelect } = props;

  function onLoadData({ key, children }) {
    return new Promise(resolve => {
      if (children) {
        resolve();
        return;
      }      
      setTimeout(() => {
          request('/api/category/tree/'+key).then(function(response) {
            setTreeData(origin =>
              updateTreeData(origin, key, response),
            );
        
        });

        resolve();
      }, 1000);
    });
  }

    

  return(
    <div className={styles.container}>         
      <Tree
        showLine={true}
        showIcon={true}
        defaultExpandedKeys={[]}
        onSelect={onSelect}
        loadData={onLoadData}
        treeData={treeData}
      />
    </div>
  );
  };
  export default CategoryTree;

