import request from '@/utils/request';
import { DataNode } from './data.d';

export async function getCatalogByParent(params?: String) {
  return request('/api/category/tree/'+params);

}

