export interface IConsignmentState 
{
  id?: number;  
}

export interface IConsignment {
    id?: number;     
    name?: string;
    price?:number;    
    quantity?:number;    
    conState?:IConsignmentState;
  }

  export interface IConUpdateParam{
    conState?:IConsignmentState;
    keys?:any[], 
  }


  export interface TableListParams {    
    status?: string;
    parentId?:number,
    name?: string;
    desc?: string;
    key?: number;
    pageSize?: number;
    currentPage?: number;
    filter?: { [key: string]: any[] };
    sorter?: { [key: string]: any };
  }