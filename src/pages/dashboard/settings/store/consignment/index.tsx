import React, { Key, useEffect, useState } from "react";
import styles from "./index.less";
import { PageContainer } from "@ant-design/pro-layout";
import { Button, message, Select, Table, Tooltip } from "antd";

import { query, update } from "./service";
import { ReloadOutlined } from "@ant-design/icons";
import { IConsignment, IConUpdateParam, TableListParams } from "./data";


// const style = { background: '#0092ff', padding: '1px 0' };

const { Option} = Select;
let conState:IConsignment={};

const Consignment : React.FC<{}> = () => {

  const [dataSource,setDataSource]=useState<IConsignment[]>([]); 
  const [selectedRowKeys , setSelectedRowKeys ] = useState<Key[]>([]);
  const [loading, setLoading ] = useState<boolean>(false);
  const [uploadText, setUploadText ] = useState<String>("Send");
  
  const conParam:IConUpdateParam={conState:{id:1}};
  
  
  //const[selectedValue,setSelectedValue]=useState({value:"1"});

  const [params,setParams]=useState<TableListParams>({
    pageSize:50,
    currentPage:1,    
  }); 

  const columns = [
    {
      title: 'Product Name',
      dataIndex: 'name',
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
    },
    {
      title: 'Price',
      dataIndex: 'price',
    },    
    {
      title: 'Total',
      dataIndex: 'option',
      valueType: 'option',
      render: (_, item) => (
      <span>{item.price*item.quantity}</span>
      ),
    },
  ];

  

  const start = async () => {
    //this.setState({ loading: true });
    // ajax request after empty completing
    setLoading(true);    
    conParam.keys=selectedRowKeys;
    conParam.conState={id:(conState.id+1)};
    const result=await update(params,conParam);
    console.log(result);      
    updateTable();
    message.success('Done');    
    setLoading(false);    
  };

  const handleChange =(value) =>{

    conState={id:parseInt(value.value)};
    //conState.id=parseInt(value.value);    
    switch(conState.id)
    {
      case 1:
        setUploadText("Send");
      break;
      case 2:
        setUploadText("Receive");
        break;
      case 3:
        setUploadText("Selling");
        break;
        case 4:
          setUploadText("Sold");
          break;
          case 5:
          setUploadText("pay");
          break;
          default:
            setUploadText("");
            break;
      
    }   
    setSelectedRowKeys([]);
    //setSelectedValue(value);        
    updateTable();
  }

  const updateTable=async()=>{    

    console.log(conState);
    query(params,conState).then(function(response) {   
      if(response)
      setDataSource(response.data) ;  
    });   

  }

  useEffect(() => {
   updateTable();
  }, [])

  return(
    <PageContainer>
      <div className={styles.container}>     
      <div style={{ marginBottom: 16 }}>
      <Select
        labelInValue
        defaultValue={{ value: '1' }}
        style={{ width: 120 }}
        onChange={handleChange}
      >
        <Option value="1">Created</Option>
        <Option value="2">Sended</Option>
        <Option value="3">Received</Option>
        <Option value="4">Selling</Option>
        <Option value="5">Sold</Option>
        <Option value="6">Payed</Option>
      </Select>
      <Tooltip title="Refresh" >
        <Button type="primary" shape="circle" icon={<ReloadOutlined />}  onClick={updateTable}/>
      </Tooltip>
      
      {
        uploadText!="" &&(
        <>
          <Button type="primary" onClick={start} disabled={!(selectedRowKeys.length>0)} loading={loading}>
            {uploadText}
          </Button>
          <span style={{ marginLeft: 8 }}>
            {(selectedRowKeys.length>0) ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
        </>)}
        
      </div>
        <Table
         rowKey={(record) => {          
          return record.id;
        }}
         rowSelection={{
                onChange: (keys, selectedRows) => {
                  setSelectedRowKeys(keys);                  
                 // console.log(keys);
                },
              }}    
               columns={columns} dataSource={dataSource} />
      </div>      
    </PageContainer>
  );
  };
  export default Consignment;

