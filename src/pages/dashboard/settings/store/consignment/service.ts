import request from '@/utils/request';
import { IConsignmentState, IConUpdateParam, TableListParams} from './data';

export async function query(params?: TableListParams,data?:IConsignmentState) {
  return request('/api/consignment/list', { 
    method: 'POST', 
    data: { 
      ...params,    
      data:{...data},        
    },
  });

}

export async function update(params?: TableListParams,data?:IConUpdateParam) {
  return request('/api/consignment/listUpdate', {
    method: 'POST',
    data: {
      ...params,
      data:{...data}, 
    },
  });
}


  