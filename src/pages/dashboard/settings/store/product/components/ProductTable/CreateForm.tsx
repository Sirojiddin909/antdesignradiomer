import React, { useEffect, useState } from 'react';
import { Button, Form, Input, message, Modal, Upload } from 'antd';
import { TableListItem } from './data';
import FormItem from 'antd/lib/form/FormItem';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons/lib/icons';

interface CreateFormProps {
  modalVisible: boolean;
  onCancel: () => void;
  onSubmit: (values:any,imgUrl:string) => void;  
  values?:TableListItem;
}

const formLayout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 13 },
};

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

const CreateForm: React.FC<CreateFormProps> = (props) => {
  const { modalVisible, onCancel,onSubmit:handleUpdate,values} = props;
  const [loading, setLoading ] = useState<Boolean>(false); 
  const [formVals, setFormVals] = useState<TableListItem>();
  const [imageUrl, setImageUrl ] = useState("");
  //  if(!(formVals.id===undefined))
  //  setImageUrl("public/category/"+formVals.id+".jpg");

  const [form] = Form.useForm();

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

 const  handleChange = info => {
    if (info.file.status === 'uploading') {
      setLoading(true);
      //this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, result =>
        {
          setLoading(false);
          setImageUrl(result);
        }    
      );
    }
  };

  const clearBeforeCancel=()=>{
    
    onCancel();
    setFormVals(undefined);   
    form.resetFields();

  }

  const beforeHandleUpdate=async ()=>{
    const fieldsValue = await form.validateFields();    
    handleUpdate({ ...formVals, ...fieldsValue },imageUrl);//fieldsValue,imageUrl);
    //onsubmit(formVals,imageUrl);
    clearBeforeCancel();
  }
  const renderFooter = () => {    
    return (
      <>
        <Button onClick={() =>{clearBeforeCancel();} }>Cancel</Button>
        <Button type="primary"  onClick={() =>{beforeHandleUpdate();} }>{formVals?.id===undefined?"Create":"Update"}</Button>
      </>
    );
  };

  useEffect(() => {
    //actionRef.current?.reload();                   
    setFormVals(values);
    setImageUrl(values?.id===undefined?"":"/public/product/img/"+values?.id+".jpg");    
    form.resetFields();  
    form.setFieldsValue({...values}); 
   // form.;
}, [values])
  

  return (
   
    <Modal
      destroyOnClose={true}
      title={formVals?.id===undefined?"New":"Update"}
      visible={modalVisible}
      onCancel={() => clearBeforeCancel()}
      footer={renderFooter()}
      forceRender

    >
      <Form
        {...formLayout}
        form={form}
        initialValues={{                    
          name: formVals?.name,                   
        }}
      >
         <FormItem
          name="name"
          label="Product Name"
          rules={[{ required: true, message: 'Must be fill！' }]}
        >
          <Input placeholder="Name of product"  />
        </FormItem>

        <FormItem
          name="description"          
          label="Description"
          rules={[{ required: true, message: 'Must be fill！' }]}
        >
          <Input placeholder="Description"  />
        </FormItem>

        <FormItem
          name="image"
          label="Image"
          // rules={[{ required: true, message: 'Must be fill！' }]}
        >
          <Upload
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              // action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
              beforeUpload={beforeUpload}
              onChange={handleChange}
            >
              {                
              imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> :uploadButton  
              }
            </Upload>
        </FormItem> 
      </Form>
    </Modal>
  );
};

export default CreateForm;
