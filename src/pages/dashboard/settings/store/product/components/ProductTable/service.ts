import request from '@/utils/request';
import { TableListParams, TableListItem } from './data';

export async function query(params?: TableListParams,fields:any) {
  return request('/api/product/get', { 
    method: 'POST', 
    data: {
      ...fields,
      data:{...params},        
    },
  });

}

export async function remove(params: TableListItem) {
  return request('/api/product/delete', {
    method: 'POST',
    data: {
      data:{...params},     
    },
  });
}

export async function add(params: TableListItem,fields:any) {
  return request('/api/product/post', {
    method: 'POST',
    data: {
      ...fields,
      data:{...params},
    },
  });
}
  