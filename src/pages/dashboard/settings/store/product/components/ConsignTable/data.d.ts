// export enum ConsignmentState{
//   CREATED=1,
//   SENDED,
//   RECEIVED,
//   SELLING,
//   SOLDED,
//   PAYED,
// }
export interface ConsignmentState{
  id?: number|undefined;
  name?: string|undefined; 
}


export interface ConsignmentItem {
    id?: number|undefined;     
    price?: number|undefined;
    quantity?:number|undefined; 
    sold?:number|undefined; 
    conState?:ConsignmentState;
  }

  export interface ConsignmentParams {
    parentId?:number;
    status?: string;
    name?: string;
    desc?: string;
    key?: number;
    pageSize?: number;
    currentPage?: number;
    filter?: { [key: string]: any[] };
    sorter?: { [key: string]: any };
  }