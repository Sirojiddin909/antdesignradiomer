import React, { useState, useRef, useEffect } from 'react';
import ProTable, { ProColumns, ActionType } from '@ant-design/pro-table';
import { ConsignmentItem } from './data';
import { Divider, Button, message, Popconfirm, Card } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { add, query, remove} from './service';
import styles from './index.less';
import CreateForm from './CreateForm';


const handleAdd = async (newRow:ConsignmentItem, fields:any) => {
  const hide = message.loading('Adding');
  try {
    await add(newRow,fields);
    hide();
    message.success('Done');
    return true;
  } catch (error) {
    hide();
    message.error('Error！');
    return false;
  }
};


/**
 *  删除节点
 * @param selectedRows
 */
const handleRemove = async (selectedRow: ConsignmentItem) => {
  const hide = message.loading('Deletiing!!!');
  if (!selectedRow) return true;
  try {
    await remove(selectedRow);
    hide();
    message.success('Deleted successfully, will refresh soon');
    return true;
  } catch (error) {
    hide();
    message.error('Deletion failed, please try again');
    return false;
  }
};

export interface TableProps{ 
  parentId?: number; 
  title?:string;
}


const ConsignTable: React.FC<TableProps> = (props) =>{
  const{parentId}=props;
  const{title}=props;  
   
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);    
  const [row, setRow] = useState<ConsignmentItem>();  
  const [confirmLoading, setConfirmLoading] = useState(false);
  const actionRef = useRef<ActionType>();
 
  const confirmDelete=async (row:ConsignmentItem)=>{
    
    setConfirmLoading(true);
    const success = await handleRemove(row);
    if (success) {      
      actionRef.current?.reload();
    }
    setConfirmLoading(false);    
  }
    const columns: ProColumns<ConsignmentItem>[] = [
        {
          title: 'Price',
          dataIndex: 'price',
          tip: 'price',
          formItemProps: {
            rules: [
              {
                required: true,
                message: 'Must be fill',
              },
            ],
          },          
        },
        {
          title: 'Quantity',
          dataIndex: 'quantity',
          tip: 'Quantity',
          formItemProps: {
            rules: [
              {
                required: true,
                message: 'Must be fill',
              },
            ],
          },
        }  
        ,
        {
          title: 'Sold',
          dataIndex: 'sold',
          tip: 'sold',
          hideInForm: true,                
        },        
        {
          title: 'Status',
          dataIndex: 'conState',
          hideInForm: true,
          key:'conState',
          render: (dom, entity) => {
          return <span>{entity.conState?.name}</span>;
          },                 
        },
        {
          title: 'Action',
          dataIndex: 'option',
          valueType: 'option',
          render: (_, record) =>{
            if(record.conState?.id==1)            
            {
                return(
                <>
                  <a onClick={() => {                
                    setRow(record);
                      handleModalVisible(true);
                    }}
                  >
                  Change
                  </a>
                  <Divider type="vertical" />
                  <Popconfirm
                  title="Are you sure to delete this task?"
                  onConfirm={()=>{
                    confirmDelete(record);                  
                  }}
                  onCancel={()=>{message.error('Click on No');}}
                  okButtonProps={{ loading: confirmLoading }}
                  okText="Yes"
                  cancelText="No"
                >
                  <a href="#">Delete</a>
                </Popconfirm>
               </>
                )
            }else

            return  (
            <></>
            )
          },
        },       
      ];
      useEffect(() => {
        actionRef.current?.reload();               
      }, [parentId])



      return (        
          <div>
            <Card title={title}  >
            <ProTable<ConsignmentItem>
              headerTitle="Consignment"
              actionRef={actionRef}
              rowKey="id"
              search={{
                labelWidth: 120,
              }}
              toolBarRender={() => [
                <Button type="primary" key="add" onClick={() =>{
                  //setSelectedRows({});
                   setRow(undefined);
                   handleModalVisible(true)
                }}>
                  <PlusOutlined />
                </Button>
                ,
              ]}
              request={(params, sorter, filter) => query  (params,{parentId })}
              columns={columns}              
              // rowSelection={{
              //   onChange: (_, selectedRows) => setSelectedRows(selectedRows),
              // }}
            />
            <CreateForm onCancel={() => {
              handleModalVisible(false)
              setRow(undefined);
            }} 
            modalVisible={createModalVisible}
              onSubmit={async (value) => {              
                const success = await handleAdd(value,{parentId,});
                if (success) {
                  handleModalVisible(false);     
                  actionRef.current?.reload();                                    
                }
              }}
              
             values={row}
            >             
           </CreateForm>
           </Card>

          </div>
      );
   
}

export default ConsignTable;