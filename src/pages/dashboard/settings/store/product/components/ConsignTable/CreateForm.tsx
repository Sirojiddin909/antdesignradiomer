import React, { useEffect, useState } from 'react';
import { Button, Form, Input, message, Modal} from 'antd';
import FormItem from 'antd/lib/form/FormItem';
import { ConsignmentItem } from './data';

interface CreateFormProps {
  modalVisible: boolean;
  onCancel: () => void;
  onSubmit: (values:any) => void;  
  values?:ConsignmentItem;
}

const formLayout = {
  labelCol: { span: 7 },
  wrapperCol: { span: 13 },
};

const CreateForm: React.FC<CreateFormProps> = (props) => {
  const { modalVisible, onCancel,onSubmit:handleUpdate,values} = props;  
  const [formVals, setFormVals] = useState<ConsignmentItem>();
  const [form] = Form.useForm();

  const clearBeforeCancel=()=>{    
    onCancel();
    setFormVals(undefined);   
    form.resetFields();
  }

  const beforeHandleUpdate=async ()=>{
    const fieldsValue = await form.validateFields();    
    handleUpdate({ ...formVals, ...fieldsValue });//fieldsValue,imageUrl);
    //onsubmit(formVals,imageUrl);
    clearBeforeCancel();
  }
  const renderFooter = () => {    
    return (
      <>
        <Button onClick={() =>{clearBeforeCancel();} }>Cancel</Button>
        <Button type="primary"  onClick={() =>{beforeHandleUpdate();} }>{formVals?.id===undefined?"Create":"Update"}</Button>
      </>
    );
  };

  useEffect(() => {
    //actionRef.current?.reload();                   
    setFormVals(values);    
    form.resetFields();  
    form.setFieldsValue({...values}); 
   // form.;
}, [values])
  

  return (
   
    <Modal
      destroyOnClose={true}
      title={formVals?.id===undefined?"New":"Update"}
      visible={modalVisible}
      onCancel={() => clearBeforeCancel()}
      footer={renderFooter()}
      forceRender

    >
      <Form
        {...formLayout}
        form={form}
        initialValues={{                    
          price: formVals?.price,
          quantity: formVals?.quantity,
        }}
      >
         <FormItem
          name="price"
          label="Product price"
          rules={[{ required: true, message: 'Must be fill！' }]}
        >
          <Input placeholder="price in $"  />
        </FormItem>

        <FormItem
          name="quantity"          
          label="Quantity"
          rules={[{ required: true, message: 'Must be fill！' }]}
        >
          <Input placeholder="Quantity"  />
        </FormItem>        
      </Form>
    </Modal>
  );
};

export default CreateForm;