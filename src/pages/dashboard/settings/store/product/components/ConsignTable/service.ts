import request from '@/utils/request';
import { ConsignmentParams, ConsignmentItem } from './data';

export async function query(params?: ConsignmentParams,fields:any) {
  return request('/api/consignment/get', { 
    method: 'POST', 
    data: {
      ...fields,
      data:{...params},        
    },
  });

}

export async function remove(params: ConsignmentItem) {
  return request('/api/consignment/delete', {
    method: 'POST',
    data: {
      data:{...params},     
    },
  });
}

export async function add(params: ConsignmentItem,fields:any) {
  return request('/api/consignment/post', {
    method: 'POST',
    data: {
      ...fields,
      data:{...params},
    },
  });
}
  