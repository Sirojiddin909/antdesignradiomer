import React, { useState } from "react";
import {Row, Col, Card} from "antd";
import styles from "./index.less";
import CategoryTree from "@/components/CategoryTree";
import ProductTable from "./components/ProductTable";
import { PageContainer } from "@ant-design/pro-layout";
import ConsignTable from "./components/ConsignTable";
import { TableListItem } from "./components/ProductTable/data";
// const style = { background: '#0092ff', padding: '1px 0' };

const Product : React.FC<{}> = () => {

  const[categoryId,setCategoryId]=useState<number>(0);
  const[categoryName,setCategoryName]=useState<string>("");

  const[productId,setProductId]=useState<number|undefined>(undefined);
  const[productName,setProductName]=useState<string|undefined>("");
  
  

   const onSelectCategory = (selectedKeys, info) => {   

        setCategoryName(info.node.title);
        setCategoryId(parseInt(info.node.key)) ;     
        //setCategoryId(parseInt(selectedKeys[0])) ;     
       
      };

      const onSelectProduct = (product:TableListItem |undefined) => {        
        setProductId(product?.id);
        setProductName(product?.name);
        // setCategoryName(info.node.title)
        // setCategoryId(parseInt(selectedKeys[0])) ;      
       
      };

  return(
    <PageContainer>
    <div className={styles.container}>     
       <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
        <Col className="gutter-row" span={productId==undefined?4:0}>
          <CategoryTree onSelect={onSelectCategory}/>
        </Col>
        <Col className="gutter-row" span={productId==undefined?20:12} >                      
            <ProductTable parentId={categoryId} title={categoryName} onSelectConsign={onSelectProduct}/>          
        </Col>        
        {productId!=undefined && (<Col className="gutter-row" span={12}>
          <ConsignTable parentId={productId} title={productName}/>
        </Col>)}
        
    </Row>
    </div>
    </PageContainer>
  );
  };
  export default Product;

