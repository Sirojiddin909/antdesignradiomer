import React, { useState } from "react";
import {Row, Col} from "antd";
import styles from "./index.less";
import CategoryTree from "@/components/CategoryTree";
import TableList from "./components/TableList";
import { PageContainer } from "@ant-design/pro-layout";
// const style = { background: '#0092ff', padding: '1px 0' };

const Category : React.FC<{}> = () => {

  const[parentId,setParentId]=useState<number>(0);

   const onSelect = (selectedKeys, info) => {
        console.log('selected', selectedKeys); 
        setParentId(parseInt(selectedKeys[0])) ;      
       
      };

  return(
    <PageContainer>
    <div className={styles.container}>     
       <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
      <Col className="gutter-row" span={6}>
         <CategoryTree onSelect={onSelect}/>
      </Col>
      <Col className="gutter-row" span={18}>
        <TableList parentId={parentId}/>
      </Col>
      
    </Row>
    </div>
    </PageContainer>
  );
  };
  export default Category;

