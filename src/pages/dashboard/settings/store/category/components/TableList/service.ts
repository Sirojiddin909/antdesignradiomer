import request from '@/utils/request';
import { TableListParams, TableListItem } from './data.d';

export async function query(params?: TableListParams,fields:any) {
  return request('/api/category/get', { 
    method: 'POST', 
    data: {
      ...fields,
      data:{...params},        
    },
  });

}

export async function remove(params: TableListItem) {
  return request('/api/category/delete', {
    method: 'POST',
    data: {
      data:{...params},     
    },
  });
}

export async function add(params: TableListItem,fields:any) {
  return request('/api/category/post', {
    method: 'POST',
    data: {
      ...fields,
      data:{...params},
    },
  });
}




 