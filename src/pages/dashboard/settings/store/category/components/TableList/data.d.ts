export interface TableListItem {
    id: number|undefined;       
    name: string|undefined;    
  }

  export interface TableListParams {
    parentId?:number;
    status?: string;
    name?: string;
    desc?: string;
    key?: number;
    pageSize?: number;
    currentPage?: number;
    filter?: { [key: string]: any[] };
    sorter?: { [key: string]: any };
  }