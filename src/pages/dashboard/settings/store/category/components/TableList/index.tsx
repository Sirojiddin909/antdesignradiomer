import React, { useState, useRef, useEffect } from 'react';
import ProTable, { ProColumns, ActionType } from '@ant-design/pro-table';
import { TableListItem } from './data.d';
import { Divider, Button, message, Popconfirm } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { add, query, remove} from './service';
import styles from './index.less';
import CreateForm from './CreateForm';



const handleAdd = async (newRow:TableListItem, fields:any) => {
  const hide = message.loading('Adding');
  try {
    await add(newRow,fields);
    hide();
    message.success('Done');
    return true;
  } catch (error) {
    hide();
    message.error('Error！');
    return false;
  }
};


/**
 *  删除节点
 * @param selectedRows
 */
const handleRemove = async (selectedRow: TableListItem) => {
  const hide = message.loading('Deletiing!!!');
  if (!selectedRow) return true;
  try {
    await remove(selectedRow);
    hide();
    message.success('Deleted successfully, will refresh soon');
    return true;
  } catch (error) {
    hide();
    message.error('Deletion failed, please try again');
    return false;
  }
};

export interface CatalogTableProps{ 
  parentId?: number; 
}


const ListTable: React.FC<CatalogTableProps> = (props) =>{
  const{parentId}=props;
   
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);    
  const [row, setRow] = useState<TableListItem>();
  //const [confirmVisible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const actionRef = useRef<ActionType>();
 
  const confirmDelete=async (row:TableListItem)=>{
    
    setConfirmLoading(true);
    const success = await handleRemove(row);
    if (success) {      
      actionRef.current?.reload();
    }
    setConfirmLoading(false);    
  }
    const columns: ProColumns<TableListItem>[] = [
        {
          title: 'Name',
          dataIndex: 'name',
          tip: 'Category Name',
          formItemProps: {
            rules: [
              {
                required: true,
                message: 'Must be fill',
              },
            ],
          },
          render: (dom, entity) => {
            return <a onClick={() => setRow(entity)}>{dom}</a>;
          },
        },
        {
          title: 'Image',
          dataIndex: 'image',
          tip: 'Image',
          valueType: 'option',
          render: (dom, entity) => {
            return <img alt="avatar"  className={styles.img} src={"/public/category/img/"+entity.id+".jpg"}></img>;
          },
        },
        {
          title: 'Action',
          dataIndex: 'option',
          valueType: 'option',
          render: (_, record) => (
            <>
              <a
                onClick={() => {                
                 setRow(record);
                  handleModalVisible(true);
                }}
              >
               Change
              </a>
              <Divider type="vertical" />
              <Popconfirm
                title="Are you sure to delete this task?"
                onConfirm={()=>{
                  confirmDelete(record);                  
                }}
                onCancel={()=>{message.error('Click on No');}}
                okButtonProps={{ loading: confirmLoading }}
                okText="Yes"
                cancelText="No"
              >
                <a href="#">Delete</a>
              </Popconfirm>
            </>
          ),
        },       
      ];
      useEffect(() => {
        actionRef.current?.reload();               
      }, [parentId])



      return (        
          <div>
            <ProTable<TableListItem>
              headerTitle="Catalog"
              actionRef={actionRef}
              rowKey="id"
              search={{
                labelWidth: 120,
              }}
              toolBarRender={() => [
                <Button type="primary" key="add" onClick={() =>{
                  //setSelectedRows({});
                   setRow(undefined);
                   handleModalVisible(true)
                }}>
                  <PlusOutlined />
                </Button>
                ,
              ]}
              request={(params, sorter, filter) => query  (params,{parentId})}
              columns={columns}              
              // rowSelection={{
              //   onChange: (_, selectedRows) => setSelectedRows(selectedRows),
              // }}
            />
            <CreateForm onCancel={() => {
              handleModalVisible(false)
              setRow(undefined);
            }} 
            modalVisible={createModalVisible}
              onSubmit={async (value,imageUrl) => {              
                const success = await handleAdd(value,{
                  image:value.image?imageUrl:null,
                  parentId,                 
                });
                if (success) {
                  handleModalVisible(false);     
                  actionRef.current?.reload(); 
                                    
                }

              }}
              
             values={row}
            >             
           </CreateForm>


          </div>
      );
   
}

export default ListTable;