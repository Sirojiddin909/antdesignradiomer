import request from 'umi-request';
export async function fakeRegister(params) {
  return request('/api/ ', {
    method: 'POST',
    data: params,
  });
}
